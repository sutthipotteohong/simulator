(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"elevator_atlas_", frames: [[0,0,1316,1635]]},
		{name:"elevator_atlas_2", frames: [[0,0,1154,1481]]},
		{name:"elevator_atlas_3", frames: [[1026,0,979,708],[716,1532,356,409],[1384,710,356,409],[1384,1121,356,409],[1432,1532,356,409],[0,0,1024,1336],[0,1338,356,409],[358,1338,356,409],[1026,710,356,409],[1074,1532,356,409],[1026,1121,356,409]]}
];


// symbols:



(lib.CachedTexturedBitmap_1 = function() {
	this.initialize(ss["elevator_atlas_3"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_10 = function() {
	this.initialize(ss["elevator_atlas_3"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_11 = function() {
	this.initialize(ss["elevator_atlas_3"]);
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_12 = function() {
	this.initialize(ss["elevator_atlas_3"]);
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_14 = function() {
	this.initialize(ss["elevator_atlas_3"]);
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_2 = function() {
	this.initialize(ss["elevator_atlas_3"]);
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_3 = function() {
	this.initialize(ss["elevator_atlas_2"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_4 = function() {
	this.initialize(ss["elevator_atlas_"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_5 = function() {
	this.initialize(ss["elevator_atlas_3"]);
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_6 = function() {
	this.initialize(ss["elevator_atlas_3"]);
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_7 = function() {
	this.initialize(ss["elevator_atlas_3"]);
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_8 = function() {
	this.initialize(ss["elevator_atlas_3"]);
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_9 = function() {
	this.initialize(ss["elevator_atlas_3"]);
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_26 = function() {
		this.gotoAndPlay(2);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(26).call(this.frame_26).wait(1));

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_5();
	this.instance.parent = this;
	this.instance.setTransform(-77,0,0.5,0.5);

	this.instance_1 = new lib.CachedTexturedBitmap_6();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-77,0,0.5,0.5);

	this.instance_2 = new lib.CachedTexturedBitmap_7();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-77,0,0.5,0.5);

	this.instance_3 = new lib.CachedTexturedBitmap_8();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-77,0,0.5,0.5);

	this.instance_4 = new lib.CachedTexturedBitmap_9();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-77,0,0.5,0.5);

	this.instance_5 = new lib.CachedTexturedBitmap_10();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-77,0,0.5,0.5);

	this.instance_6 = new lib.CachedTexturedBitmap_11();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-77,0,0.5,0.5);

	this.instance_7 = new lib.CachedTexturedBitmap_12();
	this.instance_7.parent = this;
	this.instance_7.setTransform(-77,0,0.5,0.5);

	this.instance_8 = new lib.CachedTexturedBitmap_14();
	this.instance_8.parent = this;
	this.instance_8.setTransform(-77,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},3).to({state:[{t:this.instance_2}]},3).to({state:[{t:this.instance_3}]},3).to({state:[{t:this.instance_4}]},3).to({state:[{t:this.instance_5}]},3).to({state:[{t:this.instance_6}]},3).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_8}]},3).to({state:[{t:this.instance_8}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-77,0,178,204.5);


// stage content:
(lib.elevator = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.CachedTexturedBitmap_1();
	this.instance.parent = this;
	this.instance.setTransform(75.1,68,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// classtext
	this.classtextnow = new cjs.Text("A", "bold 120px 'Roboto'", "#66CCFF");
	this.classtextnow.name = "classtextnow";
	this.classtextnow.textAlign = "center";
	this.classtextnow.lineHeight = 143;
	this.classtextnow.lineWidth = 484;
	this.classtextnow.parent = this;
	this.classtextnow.setTransform(317.9,531.25);

	this.classtext = new cjs.Text("A", "bold 100px 'Roboto'", "#66CCFF");
	this.classtext.name = "classtext";
	this.classtext.textAlign = "center";
	this.classtext.lineHeight = 119;
	this.classtext.lineWidth = 266;
	this.classtext.parent = this;
	this.classtext.setTransform(430.8,220.35);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.classtext},{t:this.classtextnow}]}).wait(1));

	// up
	this.up = new lib.Symbol2();
	this.up.name = "up";
	this.up.parent = this;
	this.up.setTransform(202.05,144.95,1,1,0,0,0,10,10);

	this.timeline.addTween(cjs.Tween.get(this.up).wait(1));

	// down
	this.down = new lib.Symbol2();
	this.down.name = "down";
	this.down.parent = this;
	this.down.setTransform(202,331.75,1,1,180,0,0,10,10);

	this.timeline.addTween(cjs.Tween.get(this.down).wait(1));

	// Layer_3
	this.instance_1 = new lib.CachedTexturedBitmap_2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(64,60,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer_1
	this.instance_2 = new lib.CachedTexturedBitmap_3();
	this.instance_2.parent = this;
	this.instance_2.setTransform(31.5,29.55,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

	// Layer_2
	this.instance_3 = new lib.CachedTexturedBitmap_4();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-8,-7.95,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(312,392.1,338,417.5);
// library properties:
lib.properties = {
	id: '38E2ADB14932466EA89FE0AD853D833C',
	width: 640,
	height: 800,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/elevator_atlas_.png?1601111972510", id:"elevator_atlas_"},
		{src:"images/elevator_atlas_2.png?1601111972510", id:"elevator_atlas_2"},
		{src:"images/elevator_atlas_3.png?1601111972511", id:"elevator_atlas_3"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['38E2ADB14932466EA89FE0AD853D833C'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;