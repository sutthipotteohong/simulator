(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"dooropen_HTML5 Canvas_atlas_", frames: [[0,0,839,1345],[380,1347,74,153],[304,1347,74,153],[228,1347,74,153],[456,1347,74,153],[152,1347,74,153],[608,1347,74,153],[0,1347,74,153],[532,1347,74,153],[76,1347,74,153],[718,1347,32,32],[684,1347,32,32],[841,1306,377,134],[841,0,660,1304]]}
];


// symbols:



(lib.CachedTexturedBitmap_1 = function() {
	this.initialize(img.CachedTexturedBitmap_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2423,1623);


(lib.CachedTexturedBitmap_2 = function() {
	this.initialize(ss["dooropen_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_23 = function() {
	this.initialize(ss["dooropen_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_24 = function() {
	this.initialize(ss["dooropen_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_25 = function() {
	this.initialize(ss["dooropen_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_26 = function() {
	this.initialize(ss["dooropen_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_27 = function() {
	this.initialize(ss["dooropen_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_28 = function() {
	this.initialize(ss["dooropen_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_29 = function() {
	this.initialize(ss["dooropen_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_30 = function() {
	this.initialize(ss["dooropen_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_31 = function() {
	this.initialize(ss["dooropen_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_37 = function() {
	this.initialize(ss["dooropen_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_41 = function() {
	this.initialize(ss["dooropen_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_6 = function() {
	this.initialize(ss["dooropen_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_7 = function() {
	this.initialize(ss["dooropen_HTML5 Canvas_atlas_"]);
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_7 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(7).call(this.frame_7).wait(8));

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_26();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.2514,0.2514);

	this.instance_1 = new lib.CachedTexturedBitmap_25();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,0,0.2514,0.2514);

	this.instance_2 = new lib.CachedTexturedBitmap_24();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,0,0.2514,0.2514);

	this.instance_3 = new lib.CachedTexturedBitmap_23();
	this.instance_3.parent = this;
	this.instance_3.setTransform(0,0,0.2514,0.2514);

	this.instance_4 = new lib.CachedTexturedBitmap_27();
	this.instance_4.parent = this;
	this.instance_4.setTransform(0,0,0.2514,0.2514);

	this.instance_5 = new lib.CachedTexturedBitmap_28();
	this.instance_5.parent = this;
	this.instance_5.setTransform(0,0,0.2514,0.2514);

	this.instance_6 = new lib.CachedTexturedBitmap_29();
	this.instance_6.parent = this;
	this.instance_6.setTransform(0,0,0.2514,0.2514);

	this.instance_7 = new lib.CachedTexturedBitmap_30();
	this.instance_7.parent = this;
	this.instance_7.setTransform(0,0,0.2514,0.2514);

	this.instance_8 = new lib.CachedTexturedBitmap_31();
	this.instance_8.parent = this;
	this.instance_8.setTransform(0,0,0.2514,0.2514);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,18.6,38.5);


(lib.connectstatus = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_9 = function() {
		this.gotoAndPlay(0);
	}
	this.frame_19 = function() {
		this.gotoAndPlay(11);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(9).call(this.frame_9).wait(10).call(this.frame_19).wait(1));

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_37();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.instance_1 = new lib.CachedTexturedBitmap_41();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[]},6).to({state:[{t:this.instance_1}]},4).to({state:[]},7).to({state:[{t:this.instance_1}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,16,16);


(lib.door = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.devicetext = new cjs.Text("{Device ID}", "bold 8px 'Roboto'", "#FFFFFF");
	this.devicetext.name = "devicetext";
	this.devicetext.textAlign = "center";
	this.devicetext.lineHeight = 11;
	this.devicetext.lineWidth = 101;
	this.devicetext.parent = this;
	this.devicetext.setTransform(87.95,168.2);

	this.devicetitle = new cjs.Text("Device ID", "bold 6px 'Roboto'", "#FFFFFF");
	this.devicetitle.name = "devicetitle";
	this.devicetitle.textAlign = "center";
	this.devicetitle.lineHeight = 9;
	this.devicetitle.lineWidth = 64;
	this.devicetitle.parent = this;
	this.devicetitle.setTransform(89.25,158.7);

	this.instance = new lib.CachedTexturedBitmap_6();
	this.instance.parent = this;
	this.instance.setTransform(33.1,150.7,0.2514,0.2514);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.devicetitle},{t:this.devicetext}]}).wait(1));

	// Layer_5
	this.led = new lib.Symbol2();
	this.led.name = "led";
	this.led.parent = this;
	this.led.setTransform(151.95,165.4,1,1,0,0,0,9.2,18.4);

	this.timeline.addTween(cjs.Tween.get(this.led).wait(1));

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AoeUeMAAAgo7IQ9AAIAAOiIjIC0IAAIWIC4CWIAAM5g");
	mask.setTransform(83.8,160.05);

	// Layer_3
	this.instance_1 = new lib.CachedTexturedBitmap_2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-48.4,-10.05,0.2514,0.2514);

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer_1
	this.instance_2 = new lib.CachedTexturedBitmap_7();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.95,-0.95,0.2514,0.2514);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.door, new cjs.Rectangle(-0.9,-0.9,165.9,327.79999999999995), null);


// stage content:
(lib.dooropen_HTML5Canvas = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.doorright.devicetext.visible = false;
		
		this.doorright.devicetitle.visible = false;
		//this.doorleft.devicetitle.visible = false;
		this.stop();
	}
	this.frame_9 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(9).call(this.frame_9).wait(11));

	// Layer_4
	this.connectstatus = new lib.connectstatus();
	this.connectstatus.name = "connectstatus";
	this.connectstatus.parent = this;
	this.connectstatus.setTransform(359,116.05,1,1,0,0,0,8,8);

	this.text = new cjs.Text("MQTT SERVER", "bold 10px 'Roboto'", "#FFFFFF");
	this.text.lineHeight = 14;
	this.text.lineWidth = 68;
	this.text.parent = this;
	this.text.setTransform(274,112.05);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text},{t:this.connectstatus}]}).wait(20));

	// doorleft
	this.doorleft = new lib.door();
	this.doorleft.name = "doorleft";
	this.doorleft.parent = this;
	this.doorleft.setTransform(429.95,463.05,1.9876,1.9876,0,0,0,82,163);

	this.timeline.addTween(cjs.Tween.get(this.doorleft).to({x:202},9).to({x:429.95},10).wait(1));

	// doorright
	this.doorright = new lib.door();
	this.doorright.name = "doorright";
	this.doorright.parent = this;
	this.doorright.setTransform(755.8,463.05,1.9888,1.9876,0,0,180,82,163);

	this.timeline.addTween(cjs.Tween.get(this.doorright).to({x:981.75},9).to({x:755.8},10).wait(1));

	// Layer_3
	this.instance = new lib.CachedTexturedBitmap_1();
	this.instance.parent = this;
	this.instance.setTransform(-3.95,-4,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(542.8,396,698.3,411.5);
// library properties:
lib.properties = {
	id: 'B8E09992A2334E93BDCFA0A8313703DA',
	width: 1200,
	height: 800,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/CachedTexturedBitmap_1.png", id:"CachedTexturedBitmap_1"},
		{src:"images/dooropen_HTML5 Canvas_atlas_.png", id:"dooropen_HTML5 Canvas_atlas_"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['B8E09992A2334E93BDCFA0A8313703DA'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;